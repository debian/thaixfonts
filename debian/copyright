Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: thaixfonts
Upstream-Contact: https://groups.google.com/group/thai-linux-foss-devel
Source: https://linux.thai.net/pub/thailinux/software/thaixfonts/

Files: *
Copyright: 2001-2015 Theppitak Karoonboonyanan
License: GPL-2+

Files: etlthai/*
Copyright: 1994 Manop Wongsaisuwan <wmanop@netserv.chula.ac.th>
License: public-domain
 Public domain font.  Share and enjoy.

Files: manop/*
Copyright: 1994 Manop Wongsaisuwan <wmanop@netserv.chula.ac.th>
License: public-domain
 Public domain font.  Share and enjoy.

Files: nectec/*
Copyright: 1999, 2000 National Electronics and Computer Technology Center
License: GPL-2+

Files: poonlap/*
Copyright: 2001 Poonlap Veerathanabutr <Poonlap.Veerathanbutr@japan.sun.com>
License: MIT
 License to copy and distribute for both commercial and
 non-commercial use is herby granted, provided this notice
 is preserved.

Files: yenbut/*
Copyright: 1992 Voradesh Yenbut <yenbut@cs.washington.edu>
License: MIT2
  These fonts are free to be redistributed as long as original copyright
  notice is retained.

Files: debian/*
Copyright: 2000-2004 Chanop Silpa-Anan <chanop@debian.org>
           2006-2023 Theppitak Karoonboonyanan <thep@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License version 2 can be found in `/usr/share/common-licenses/GPL-2'.
