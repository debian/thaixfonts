Source: thaixfonts
Section: fonts
Priority: optional
Maintainer: Theppitak Karoonboonyanan <thep@debian.org>
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: xfonts-utils
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/thaixfonts.git
Vcs-Browser: https://salsa.debian.org/debian/thaixfonts
Homepage: https://linux.thai.net/projects/thaixfonts
Standards-Version: 4.6.2

Package: xfonts-thai
Architecture: all
Multi-Arch: foreign
Depends:
 fonts-thai-tlwg,
 xfonts-thai-etl,
 xfonts-thai-manop,
 xfonts-thai-nectec,
 ${misc:Depends}
Recommends: fonts-arundina, xfonts-thai-poonlap, xfonts-thai-vor
Suggests: xfonts-intl-asian, xserver
Description: Collection of Thai fonts for X (metapackage)
 This is a metapackage which depends on or recommends all available Thai
 fonts packages.

Package: xfonts-thai-nectec
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: xserver
Description: Thai fixed fonts for X from Nectec
 This package provides one set of Thai TIS-620 pseudo-monospace (with
 zero-width combining character) bitmap fonts for X developed by NECTEC, with
 complete set of normal, bold, italic, and bold-italic faces.
 .
 This font looks good in xiterm+thai, with complete text styles support.

Package: xfonts-thai-etl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: xserver
Description: Thai etl fonts for X
 This package provides 3 Thai TIS-620 monospace (with full-width combining
 characters) bitmap fonts for X developed by ETL.
 .
 Emacs/Mule needs these fonts to display Thai. Only normal weight is provided,
 though.

Package: xfonts-thai-manop
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: xserver
Description: Manop Wongsaisuwan's bitmap fonts for X
 This package provides 4 Thai TIS-620 pseudo-monospace (with zero-width
 combining characters) bitmap fonts for X developed by Dr. Manop Wongsaisuwan
 while he was in the US.
 .
 These fonts are good for xiterm+thai, but with only normal weight provided.

Package: xfonts-thai-vor
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: xserver
Description: Voradesh Yenbut's bitmap fonts for X
 This package provides two TIS-620 pseudo-monospace (with zero-width combining
 characters) bitmap fonts for X developed by Voradesh Yenbut back in 1992.
 .
 These fonts are good for xiterm+thai, but with only normal weight provided.

Package: xfonts-thai-poonlap
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: xserver
Description: Poonlap Veerathanabutr's bitmap fonts for X
 This package provides 14 Thai TIS-620 pseudo-monospace (with zero-width
 combining characters) and monospace (with full-width combining characters)
 bitmap fonts for X and Emacs.
 .
 These fonts are good for xiterm+thai and Emacs, with complete text styles
 support for most fonts.
